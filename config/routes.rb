Rails.application.routes.draw do
  devise_for :users do
  get '/users/sign_out' => 'devise/sessions#destroy'
end
  resources :medicinas
  resources :meetings
  resources :events
  resources :personas

  root to: 'personas#index'

  resources :personas do
  resources :medicinas
end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
