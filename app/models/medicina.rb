class Medicina < ApplicationRecord
    belongs_to :persona

    validates :nombre, :aplicacion, :descripcion, :fecha, presence: true
end
