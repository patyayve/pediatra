class Persona < ApplicationRecord
   has_many :medicinas
    
    mount_uploader :foto, FotoUploader
    validates :contacto, length: {maximum: 10}
    validates :edad,  numericality: {only_integer: true, message: "Sólo números!!"}
    validates_format_of :email,:with => /\A[^@\s]+@([^@\s]+\.)+[^@\s]+\z/
    validates :direccion, :descripcion, :peso, :altura, :padre, presence: true

def self.search(search)
  where("nombre LIKE ? OR edad LIKE ? OR padre LIKE ? OR descripcion LIKE ?
   OR peso LIKE ? OR altura LIKE ? ",
  "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%" ) 
end
end
