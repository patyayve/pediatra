json.extract! persona, :id, :nombre, :edad, :padre, :descripcion, :peso, :altura, :foto, :direccion, :email, :contacto, :created_at, :updated_at
json.url persona_url(persona, format: :json)
