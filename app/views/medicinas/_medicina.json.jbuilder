json.extract! medicina, :id, :nombre, :aplicacion, :descripcion, :fecha, :created_at, :updated_at
json.url medicina_url(medicina, format: :json)
