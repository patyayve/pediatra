class PersonasController < ApplicationController
  before_action :set_persona, only: [:show, :edit, :update, :destroy]

  # GET /personas
  # GET /personas.json
  def index
    @personas = Persona.order(:padre)
    if params[:search]
      @personas = Persona.search(params[:search]).order(:padre)
    else
      @personas = Persona.all.order(:padre)
    end
  end

class PersonaPdf < Prawn::Document
    def initialize(persona)
        super(top_margin: 70, left_margin: 50, right_margin: 50)
        @persona = persona
        persona_id
        
    end

    def persona_id
      pdf = Prawn::Document.new
      text "paciente #{@persona.nombre}", size:30, style: :bold
      text ("")
      text "____________________________________________________________________________", size: 12
      text "\n"
      text "Edad: ", size: 14, style: :bold
      text "#{@persona.edad}", size: 12
      text "\n"
      text "Descripción: ", size: 14, style: :bold
      text "#{@persona.descripcion}", size: 12
      text "\n"
      text "Peso: ", size: 14, style: :bold
      text "#{@persona.peso}", size: 12
      text "\n"
      text "Altura: ", size: 14, style: :bold
      text "#{@persona.altura}", size: 12
      text "\n"
      text "Padre: ", size: 14, style: :bold
      text "#{@persona.padre}", size: 12
      text "\n"
      text "Dirrección: ", size: 14, style: :bold
      text "#{@persona.direccion}", size: 12
      text "\n"
      text "Telefono: ", size: 14, style: :bold
      text "#{@persona.contacto}", size: 12
      end    
end
  # GET /personas/1
  # GET /personas/1.json
  def show
    @persona = Persona.find(params[:id])
    respond_to do |format|
      format.html
      format.pdf do
        pdf = PersonaPdf.new(@persona)
        send_data pdf.render, filename: "persona_#{@persona.nombre}.pdf",
                              type: "application/pdf",
                              disposition: "inline"
        end
      end
  end

  # GET /personas/new
  def new
    @persona = Persona.new
  end

  # GET /personas/1/edit
  def edit
  end

  # POST /personas
  # POST /personas.json
  def create
    @persona = Persona.new(persona_params)

    respond_to do |format|
      if @persona.save
        format.html { redirect_to @persona, notice: 'el paciente fue creada con exito.' }
        format.json { render :show, status: :created, location: @persona }
      else
        format.html { render :new }
        format.json { render json: @persona.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /personas/1
  # PATCH/PUT /personas/1.json
  def update
    respond_to do |format|
      if @persona.update(persona_params)
        format.html { redirect_to @persona, notice: 'el paciente fue editado con exito' }
        format.json { render :show, status: :ok, location: @persona }
      else
        format.html { render :edit }
        format.json { render json: @persona.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /personas/1
  # DELETE /personas/1.json
  def destroy
    @persona.destroy
    respond_to do |format|
      format.html { redirect_to personas_url, notice: 'el paciente fue eliminado con exito ' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_persona
      @persona = Persona.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def persona_params
      params.require(:persona).permit(:nombre, :edad, :padre, :descripcion, :peso, :altura, :foto, :direccion, :email, :contacto)
    end
end
