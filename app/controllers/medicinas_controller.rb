class MedicinasController < ApplicationController
  
  
  def index
    @medicinas = Medicina.all
  end


  def create
    @persona = Persona.find(params[:persona_id])
    @medicina = @persona.medicinas.create(medicina_params)
    redirect_to persona_path(@persona)
  end

  
  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def medicina_params
      params.require(:medicina).permit(:nombre, :aplicacion, :descripcion, :fecha)
    end
end
