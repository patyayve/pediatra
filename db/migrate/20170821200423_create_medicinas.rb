class CreateMedicinas < ActiveRecord::Migration[5.1]
  def change
    create_table :medicinas do |t|
      t.string :nombre
      t.string :aplicacion
      t.string :descripcion
      t.date :fecha

      t.timestamps
    end
  end
end
