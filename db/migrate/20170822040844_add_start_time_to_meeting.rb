class AddStartTimeToMeeting < ActiveRecord::Migration[5.1]
  def change
    add_column :meetings, :start_time, :datetime
  end
end
