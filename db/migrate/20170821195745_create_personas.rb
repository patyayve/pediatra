class CreatePersonas < ActiveRecord::Migration[5.1]
  def change
    create_table :personas do |t|
      t.string :nombre
      t.integer :edad
      t.string :padre
      t.string :descripcion
      t.float :peso
      t.float :altura
      t.string :foto
      t.string :direccion
      t.string :email
      t.string :contacto

      t.timestamps
    end
  end
end
