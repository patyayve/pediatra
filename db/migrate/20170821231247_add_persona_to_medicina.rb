class AddPersonaToMedicina < ActiveRecord::Migration[5.1]
  def change
    add_reference :medicinas, :persona, foreign_key: true
  end
end
